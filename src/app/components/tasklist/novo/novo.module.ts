import {NgModule} from '@angular/core';
import {MainModule} from 'sofisa-core';
import {CommonModule} from '@angular/common';
import {NovoComponent} from './novo.component';
import {TaskFormModule} from 'sofisa-bpm';
import {NovoRoutingModule} from './novo-routing.module';
import {ReactiveFormsModule} from '@angular/forms';

/**
	Module responsável pela página `Criar uma task`.

	Módulos importados:

	- MainModule
	- TaskFormModule,

	RoutingModule: 
	
	- NovoRoutingModule
*/

@NgModule({
	declarations: [NovoComponent],
	imports: [
		CommonModule,
		MainModule,
		TaskFormModule,
		ReactiveFormsModule,
		NovoRoutingModule
	],
	exports: [NovoComponent]
})

export class NovoModule {}
