import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NovoComponent } from './novo.component';

const routes: Routes = [
	{
		path: '',
		component: NovoComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class NovoRoutingModule {}
