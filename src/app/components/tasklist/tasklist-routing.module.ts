import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasklistComponent } from './tasklist.component';

const routes: Routes = [
	{
		path: '',
		component: TasklistComponent
	},
	{
		path: 'novo',
		loadChildren: './novo/novo.module#NovoModule'
	},
	{
		path: 'editar/:id',
		loadChildren: './editar/editar.module#EditarModule'
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class TasklistRoutingModule {}
