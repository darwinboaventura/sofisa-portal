import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {BpmService} from 'sofisa-bpm';
import {Store, select} from '@ngrx/store';
import {LoaderService} from 'sofisa-loader';
import {SetAllTasks, handleError} from 'sofisa-core';
import {NotificationService} from 'sofisa-notification';
import {AfterViewInit, Component, OnInit} from '@angular/core';

/**
	Carrega o component `Tasklist` e o alimenta através da propriedade `props`.
*/

@Component({
	selector: 'app-tasklist',
	templateUrl: './tasklist.component.html',
	styleUrls: ['./tasklist.component.scss']
})

export class TasklistComponent implements OnInit, AfterViewInit {
	tasks$: Observable<Array<any>>;

	state: any = {
		tasks: []
	};
	
	/**
	 * Atribui o observable do store `pages` para a propriedade `tasks$`.
	 * Exibe o loader até que seja finalizado as requisições necessárias.
	 */
	constructor(public router: Router, public bpmService: BpmService, public notificationService: NotificationService, public store: Store<{ pages: any, loader: any }>, public loaderService: LoaderService) {
		this.tasks$ = store.pipe(select('pages'));

		this.loaderService.addAnItemToLoader({
			page: 'tasklist',
			name: 'tasks'
		});
	}
	
	/**
	 * Escuta por alterações no store `pages` e atribui para a `state.tasks`.
	 */
	ngOnInit() {
		this.tasks$.subscribe((store: any) => {
			this.state.tasks = store.pages.tasklist.tasks;
		});
	}
	
	/**
	 * Utiliza o método `getTasks` do serviço `BpmService` para solicitar as tarefas associadas ao usuário logado.
	 * 
	 * Caso possua tarefas, utiliza a action `SetAllTasks` para atualizar o estado.
	 * 
	 * Após finalizar a requisição, oculta o loader.
	 */
	ngAfterViewInit() {		
		this.bpmService.getTasks().subscribe(
			(res: any) => {
				this.store.dispatch(new SetAllTasks({
					tasks: res
				}));
				
				this.loaderService.removeAnItemToLoader({
					page: 'tasklist',
					name: 'tasks'
				});
			},
			(err) => {
				handleError.call(this, err, { page: 'tasklist', name: 'tasks'});
			}
		);
	}
	
	/**
	 * Responsável por direcionar para a rota `tasklist/novo` ao clicar no botão `Criar uma task`
	 */
	goToCreatePage(e) {
		this.router.navigateByUrl('tasklist/novo');
	}
}
