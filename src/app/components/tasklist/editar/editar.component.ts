import {Observable} from 'rxjs';
import {BpmService} from 'sofisa-bpm';
import {OAuthService} from 'angular-oauth2-oidc';
import {Store, select} from '@ngrx/store';
import {LoaderService} from 'sofisa-loader';
import {NotificationService} from 'sofisa-notification';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AfterViewChecked, Component, OnInit} from '@angular/core';
import {formatDateToInput, SetTaskBeingEdited, SetUserOnTaskBeingEdited, UpdateActionsOnTaskForm, handleFieldChange, handleError} from 'sofisa-core';

/**
	Carrega o component `TaskForm` e o alimenta através da propriedade `props`. Recebe os valores preenchidos através do evento `submission`.
*/

@Component({
	selector: 'app-editar',
	templateUrl: './editar.component.html',
	styleUrls: ['./editar.component.scss']
})

export class EditarComponent implements OnInit, AfterViewChecked {
	pages$: Observable<any>;
	loader$: Observable<any>;
	
	state: any = {
		page: 'tasklist/editar',
		task: {
			id: null,
			processDefinitionId: null,
			diagram: null,
			name: '',
			type: '',
			claimed: false,
			claimedBy: ''
		},
		user: {
			id: null
		},
		form: {
			formGroup: this.fb.group({
				variables: this.fb.group({
					cliente: this.fb.group({
						value: [null, Validators.required],
						type: ['String'],
						valueInfo: [{}]
					}),
					cnpj: this.fb.group({
						value: [null, Validators.required],
						type: ['String'],
						valueInfo: [{}]
					}),
					dataVisita: this.fb.group({
						value: [null, Validators.required],
						type: ['String'],
						valueInfo: [{}]
					}),
					aprovadoT3: this.fb.group({
						value: ['', Validators.required],
						type: ['String'],
						valueInfo: [{}]
					}),
					aprovadoT4: this.fb.group({
						value: ['', Validators.required],
						type: ['String'],
						valueInfo: [{}]
					})
				})
			}),
			validations: {
				cliente: {
					type: '',
					message: ''
				},
				cnpj: {
					type: '',
					message: ''
				},
				dataVisita: {
					type: '',
					message: ''
				}
			},
			actions: {
				submitted: false
			}
		}
	};
	
	/**
	 * Atribui o observable `Store pages` a propriedade `pages$`.
	 * Carrega o loader
	 */
	constructor(public fb: FormBuilder, public bpmService: BpmService, public router: Router, public route: ActivatedRoute, public notificationService: NotificationService, public store: Store<{ pages: any, loader: any }>, public oauthService: OAuthService, public loaderService: LoaderService) {		
		this.pages$ = store.pipe(select('pages'));

		this.loaderService.addAnItemToLoader({
			page: 'tasklist/editar',
			name: 'diagram'
		});
	}
	
	/**
	 * Atribui os dados do usuário para a variabel `localUser`.
	 * Caso os dados existam atualiza o usuário sendo editado, utilizando da action `SetUserOnTaskBeingEdited`.
	 * 
	 * Escuta por mudanças nos parametro da rota. 
	 * 
	 * Caso ocorra alguma alteração:
	 * 
	 * - Utiliza o método `getTaskById` do serviço `BpmService` para receber os dados da tarefa. Após receber os dados, atualiza:
	 * 	- id, utilizando a action SetTaskBeingEdited
	 * 	- processDefinitionId, utilizando a action SetTaskBeingEdited
	 * 	- name, utilizando a action SetTaskBeingEdited
	 * 	- claimedBy, utilizando a action SetTaskBeingEdited
	 * 	- claimed, utilizando a action SetTaskBeingEdited
	 */
	ngOnInit() {
		const localUser = JSON.parse(localStorage.getItem('user'));
		
		if (localUser && localUser.user) {
			this.store.dispatch(new SetUserOnTaskBeingEdited({
				user: localUser.user
			}));
		}
		
		this.route.params.subscribe((params: any) => {
			this.store.dispatch(new SetTaskBeingEdited({
				fieldName: 'id',
				fieldValue: params.id
			}));
			
			this.bpmService.getTaskById(params.id).subscribe((res: any) => {
				this.store.dispatch(new SetTaskBeingEdited({
					fieldName: 'processDefinitionId',
					fieldValue: res.processDefinitionId
				}));
				
				this.store.dispatch(new SetTaskBeingEdited({
					fieldName: 'name',
					fieldValue: res.name
				}));
				
				this.store.dispatch(new SetTaskBeingEdited({
					fieldName: 'claimedBy',
					fieldValue: res.assignee
				}));

				if (this.state.task.claimedBy && this.state.user.id) {
					this.store.dispatch(new SetTaskBeingEdited({
						fieldName: 'claimed',
						fieldValue: this.state.task.claimedBy === this.state.user.id
					}));
				}
				
				this.bpmService.getTaskDiagram(this.state.task.processDefinitionId).subscribe((diagram: any) => {				
					if (diagram && diagram.bpmn20Xml) {
						this.state.task.diagram = diagram.bpmn20Xml;
					}

					this.loaderService.removeAnItemToLoader({
						page: 'tasklist/editar',
						name: 'diagram'
					});
				}, (err: any) => {
					handleError.call(this, err, {page: 'tasklist/editar', name: 'diagram'});
				});
			}, (err) => {
				handleError.call(this, err, { 
					page: 'tasklist/editar',
					name: 'task'
				 });
			});

			this.loaderService.addAnItemToLoader({
				page: 'tasklist/editar',
				name: 'variables'
			});
			
			this.bpmService.getTaskByIdVariables(params.id).subscribe((res: any) => {
				if (res.cliente) {
					handleFieldChange(this.state.form.formGroup, this.store, 'tasklist/editar', res.cliente.value, 'cliente');
				}
				
				if (res.cnpj) {
					handleFieldChange(this.state.form.formGroup, this.store, 'tasklist/editar', res.cnpj.value, 'cnpj');
				}
				
				if (res.dataVisita) {
					handleFieldChange(this.state.form.formGroup, this.store, 'tasklist/editar', formatDateToInput(res.dataVisita.value, 2), 'dataVisita');
				}
				
				this.loaderService.removeAnItemToLoader({
					page: 'tasklist/editar',
					name: 'variables'
				});
			}, (err) => {
				handleError.call(this, err, { 
					page: 'tasklist/editar',
					name: 'variables'
				 });
			});
		});
		
		this.pages$.subscribe((store: any) => {
			this.state.user = store.pages[this.state.page].user;
			this.state.task = store.pages[this.state.page].task;
		});
	}
	
	/**
	 * Verifica se o campo `this.state.task.name` é válido, caso seja:
	 * 
	 * - Verifica o tipo de tarefa
	 * - Baseado no tipo, adiciona um campo ao formulário
	 */
	ngAfterViewChecked() {
		if (this.state.task.name) {
			const rootGroup = this.state.form.formGroup.get('variables').controls;
			
			if (this.state.task.name.includes('Comercial')) {
				delete rootGroup.aprovadoT3;
				
				this.store.dispatch(new SetTaskBeingEdited({
					fieldName: 'type',
					fieldValue: 'comercial'
				}));
			} else if (this.state.task.name.includes('Crédito')) {
				delete rootGroup.aprovadoT4;
				
				this.store.dispatch(new SetTaskBeingEdited({
					fieldName: 'type',
					fieldValue: 'credito'
				}));
			}
		}
		
		this.state.form.formGroup.updateValueAndValidity();
	}
	
	/**
	 * Carrega o loader.
	 * Utiliza o método `claimTaskById` do serviço `BpmService`.
	 * Após finalização a requisição, remove o loader, adiciona mensagem de sucesso e atualiza o valor de `claimed`
	 * 
	 * @param data Objeto com as propriedades `taskId` e `userId`
	 */
	handleClaim(data: { taskId: any, userId: any }) {
		this.loaderService.addAnItemToLoader({
			page: 'tasklist/editar',
			name: 'claim'
		});
		
		this.bpmService.claimTaskById(data.taskId, data.userId).subscribe((res: any) => {
			this.loaderService.removeAnItemToLoader({
				page: 'tasklist/editar',
				name: 'claim'
			});
			
			this.notificationService.addMessage({
				type: 'success',
				message: 'A tarefa foi associada ao seu usuário.'
			});
			
			this.store.dispatch(new SetTaskBeingEdited({
				fieldName: 'claimed',
				fieldValue: true
			}));
		}, (err: any) => {
			handleError.call(this, err, { page: 'tasklist/editar', name: 'claim' });
		});
	}
	
	/**
	 * UpadteTask: 
	 * 
	 * - Atualiza valor da propriedade submitted
	 * - Verifica se o formulário é válido, caso seja válido:
	 * 	- Exibe o loader
	 * 	- Utiliza o método `completeTaskById` do serviço `BpmService`
	 * 	- Ao finalizar a requisição, direciona para a rota 'tasklist'
	 * 	- Adiciona mensagem de sucesso
	 * 	- Remove modal
	 * 
	 * @param formGroup Instancia do formulário passado para o `TaskForm`
	 */
	updateTask(formGroup: FormGroup) {
		this.store.dispatch(new UpdateActionsOnTaskForm({
			page: 'tasklist/editar',
			actionName: 'submitted',
			actionValue: true
		}));
		
		if (formGroup.valid) {
			this.loaderService.addAnItemToLoader({
				page: 'tasklist/editar',
				name: 'complete'
			});
			
			this.bpmService.completeTaskById(this.state.task.id, formGroup.value).subscribe(
				(res: any) => {
					this.router.navigateByUrl('/tasklist');
					
					this.notificationService.addMessage({
						type: 'success',
						message: 'A tarefa foi concluida com sucesso!'
					});

					this.loaderService.removeAnItemToLoader({
						page: 'tasklist/editar',
						name: 'complete'
					});
				},
				(err) => {
					handleError.call(this, err, {page: 'tasklist/editar', name: 'complete'});

					this.loaderService.removeAnItemToLoader({
						page: 'tasklist/editar',
						name: 'complete'
					});
				}
			);
		}
	}
}
