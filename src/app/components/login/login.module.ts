import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginFormModule } from 'sofisa-login';
import { LoginRoutingModule } from './login-routing.module';
import { CheckboxModule, ButtonModule } from 'sofisa-ui';

/**
	Módulo responsável pela página `Login`.

	Importa os módulos `CheckboxModule`, `LoginFormModule` e `ButtonModule`.

	Ele carrega o módulo de rota `LoginRoutingModule`. Esse é responsável por especificar as rotas e qual componente carregar ao acessa-lás. 
*/

@NgModule({
	declarations: [LoginComponent],
	imports: [
		CommonModule,
		LoginRoutingModule,
		CheckboxModule,
		LoginFormModule,
		ButtonModule
	],
	exports: [LoginComponent]
})

export class LoginModule { }
