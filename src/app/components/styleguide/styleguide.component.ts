import {Component, OnInit} from '@angular/core';

/**
	Utiliza os components do `sofisa-ui` para montar a página `Styleguide`.

	Sua função é exibir todos os componentes de UI já montados.
*/

@Component({
    selector: 'app-styleguide',
    templateUrl: './styleguide.component.html',
    styleUrls: ['./styleguide.component.scss']
})

export class StyleguideComponent implements OnInit {
    constructor() {}
    
    ngOnInit() {}
}
