import {Observable} from 'rxjs';
import {Store, select} from '@ngrx/store';
import {Injectable, Inject} from '@angular/core';
import {CleanNotification, AddANotification} from '../../ngrx/actions/notifications/notifications.actions';

@Injectable({
	providedIn: 'root'
})

export class NotificationService {
	public state: any = {
		type: '',
		message: '',
		displaying: false
	};

	store$: Observable<any>;

	constructor(@Inject(Store) public store) {
		this.store$ = store.pipe(select('notification'));

		this.store$.subscribe((state: any) => {
			this.state = state;
		});
	}
	
	cleanMessages() {
		this.store.dispatch(new CleanNotification());
	}
	
	addMessage(notification: { type: string, message: string }) {
		this.store.dispatch(new AddANotification(notification));
	}
}
