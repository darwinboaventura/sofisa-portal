import {Action} from '@ngrx/store';

export enum NotificationTypes {
	AddANotification = '[AppComponent] AddANotification',
	CleanNotification = '[AppComponent] CleanNotification'
}

export class AddANotification implements Action {
	readonly type = NotificationTypes.AddANotification;
	
	constructor(public payload: { type: any, message: any}) {}
}

export class CleanNotification implements Action {
	readonly type = NotificationTypes.CleanNotification;
}

