import {Component} from '@angular/core';
import {CheckboxComponent} from '../checkbox/checkbox.component';

@Component({
	selector: 'so-radiobox',
	templateUrl: './radiobox.component.html',
	styleUrls: ['./radiobox.component.scss']
})

export class RadioboxComponent extends CheckboxComponent {}
