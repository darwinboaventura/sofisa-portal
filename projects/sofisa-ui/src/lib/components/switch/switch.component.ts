import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
	selector: 'so-switch',
	templateUrl: './switch.component.html',
	styleUrls: ['./switch.component.scss']
})

export class SwitchComponent implements OnInit {
	@Input() isActive: Boolean = false;
	@Output() hasChange: EventEmitter<Boolean> = new EventEmitter();
	
	constructor() {}
	
	ngOnInit() {}
	
	toggleSwitch() {
		this.isActive = !this.isActive;
		
		this.hasChange.emit(this.isActive);
	}
}
