import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
	selector: 'so-textarea',
	templateUrl: './textarea.component.html',
	styleUrls: ['./textarea.component.scss']
})

export class TextareaComponent {
	@Input() label: String = '';
	@Input() value: String = '';
	
	@Output() hasChanged: EventEmitter<any> = new EventEmitter();
	
	handleChange(e) {
		this.hasChanged.emit(e.target.value);
	}
}
