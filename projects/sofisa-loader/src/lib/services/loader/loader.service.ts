import { Injectable, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {Observable} from 'rxjs';
import {AddLoaderItem, RemoveLoaderItem} from '../../ngrx/actions/loader/loader.actions';

@Injectable({
	providedIn: 'root'
})

export class LoaderService {
	public store$: Observable<any>;
	public isActive: boolean;
	
	constructor(@Inject(Store) public store) {
		this.store$ = store.pipe(select('loader'));

		this.store$.subscribe((state: any) => {
			this.isActive = Boolean(state.length);
		});
	}

	addAnItemToLoader(payload: { page: string, name: string }) {
		this.store.dispatch(new AddLoaderItem(payload));
	}
	
	removeAnItemToLoader(payload: { page: string, name: string }) {
		this.store.dispatch(new RemoveLoaderItem(payload));
	}
}
