import {Action} from '@ngrx/store';

export enum LoaderTypes {
	AddLoaderItem = '[AppComponent] AddLoaderItem',
	RemoveLoaderItem = '[AppComponent] RemoveLoaderItem'
}

export class AddLoaderItem implements Action {
	readonly type = LoaderTypes.AddLoaderItem;
	
	constructor(public payload: { page: string, name: string }) {}
}

export class RemoveLoaderItem implements Action {
	readonly type = LoaderTypes.RemoveLoaderItem;
	
	constructor(public payload: { page: string, name: string }) {}
}

