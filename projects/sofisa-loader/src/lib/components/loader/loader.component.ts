import {Component, Input} from '@angular/core';

@Component({
	selector: 'so-loader',
	templateUrl: './loader.component.html',
	styleUrls: ['./loader.component.scss']
})

export class LoaderComponent {
	@Input() show: Boolean = false;
}
