import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AsideComponent} from './aside.component';

@NgModule({
	declarations: [AsideComponent],
	imports: [
		CommonModule,
		RouterModule
	],
	exports: [AsideComponent]
})
export class AsideModule { }
