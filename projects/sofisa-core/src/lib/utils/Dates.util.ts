export function formatDateToInput(value: any, type: number = 1) {
	const separator = value.includes('-') ? '-' : '/';
	
	let pieces;
	let formated;
	
	if (type === 1) {
		pieces = value.split(separator);
		formated = `${pieces[0]}-${pieces[1]}-${pieces[2]}`;
	} else if (type === 2) {
		pieces = value.split(separator);
		formated = `${pieces[2]}-${pieces[1]}-${pieces[0]}`;
	}
	
	if (formated && formated.length === 10) {
		formated = formated.substring(0, 10);
	}
	
	return formated;
}

export function formatDateToApiFormat(value: string) {
	const separator = value.includes('-') ? '-' : '/';
	const breakedDate = value.split(separator);
	const data = `${breakedDate[2]}/${breakedDate[1]}/${breakedDate[0]}`;
	
	return data;
}
