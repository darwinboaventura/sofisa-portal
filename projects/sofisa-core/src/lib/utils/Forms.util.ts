import {FormGroup} from '@angular/forms';
import {SetValidationOnTaskForm} from '../ngrx/actions/tasks/tasks.actions';

export function handleFieldChange(formGroup: FormGroup, store: any, page: any, value: any, name: any) {
	if (value) {
		formGroup.get(`variables.${name}.value`).setValue(value);
		
		store.dispatch(new SetValidationOnTaskForm({
			page: page,
			fieldName: name,
			validationType: '',
			validationMessage: ''
		}));
	} else if (this.props.form.actions.submitted) {
		store.dispatch(new SetValidationOnTaskForm({
			page: page,
			fieldName: name,
			validationType: 'error',
			validationMessage: 'Campo obrigatório'
		}));
	}
}
