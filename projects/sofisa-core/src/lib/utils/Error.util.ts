import {HttpErrorResponse} from '@angular/common/http';

export function handleError(err: any, loader: { page: string, name: string}) {
	console.log('Error:', err);
	
	this.loaderService.removeAnItemToLoader(loader);
	
	if (err instanceof HttpErrorResponse) {
		this.notificationService.addMessage({
			type: 'error',
			message: err.error.message || err.error.error_description
		});
	} else {
		this.notificationService.addMessage({
			type: 'error',
			message: err.message
		});
	}
}

