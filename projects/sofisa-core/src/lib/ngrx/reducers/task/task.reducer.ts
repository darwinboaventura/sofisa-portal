import {ActionTypes} from '../../actions/tasks/tasks.actions';

export const defaultState = {
	'tasklist': {
		'tasks': []
	},
	'tasklist/novo': {
		'form': {
			'validations': {
				'cliente': {
					'type': '',
					'message': ''
				},
				'cnpj': {
					'type': '',
					'message': ''
				},
				'dataVisita': {
					'type': '',
					'message': ''
				}
			},
			'actions': {
				'submitted': false
			}
		}
	},
	'tasklist/editar': {
		'task': {
			'id': null,
			'name': '',
			'type': '',
			'claimed': false,
			'claimedAuthor': ''
		},
		'user': {
			'id': null
		},
		'form': {
			'validations': {
				'cliente': {
					'type': '',
					'message': ''
				},
				'cnpj': {
					'type': '',
					'message': ''
				},
				'dataVisita': {
					'type': '',
					'message': ''
				}
			},
			'actions': {
				'submitted': false
			}
		}
	}
};

export function taskReducer(state = defaultState, action: any) {
	switch (action.type) {
		case ActionTypes.CreateTask:
			const tasks = state.tasklist.tasks;
			tasks.push(action.payload.task);
			
			return Object.assign({}, state, {
				tasklist: {
					...state.tasklist,
					tasks: tasks
				}
			});
		case ActionTypes.SetAllTasks:
			return Object.assign({}, state, {
				tasklist: {
					...state.tasklist,
					tasks: action.payload.tasks
				}
			});
		case ActionTypes.SetValidationOnTaskForm:
			return Object.assign({}, state, {
				[action.payload.page]: {
					task: state[action.payload.page].task,
					user: state[action.payload.page].user,
					form: {
						actions: state[action.payload.page].form.actions,
						validations: {
							...state[action.payload.page].form.validations,
							[action.payload.fieldName]: {
								type: action.payload.validationType,
								message: action.payload.validationMessage
							}
						}
					}
				}
			});
		case ActionTypes.SetUserOnTaskBeingEdited:
			return Object.assign({}, state, {
				'tasklist/editar': {
					...state['tasklist/editar'],
					user: action.payload.user
				}
			});
		case ActionTypes.UpdateActionsOnTaskForm:
			return Object.assign({}, state, {
				[action.payload.page]: {
					...state[action.payload.page],
					form: {
						...state[action.payload.page].form,
						actions: {
							...state[action.payload.page].form.actions,
							[action.payload.actionName]: action.payload.actionValue
						}
					}
				}
			});
		case ActionTypes.SetTaskBeingEdited:
			return Object.assign({}, state, {
				'tasklist/editar': {
					...state['tasklist/editar'],
					task: {
						...state['tasklist/editar'].task,
						[action.payload.fieldName]: action.payload.fieldValue
					}
				}
			});
		default:
			return state;
	}
}


