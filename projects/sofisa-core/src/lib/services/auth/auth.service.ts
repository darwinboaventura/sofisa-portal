import {Store} from '@ngrx/store';
import {handleError} from '../../utils/Error.util';
import {OAuthService} from 'angular-oauth2-oidc';
import {LoaderService} from 'sofisa-loader';
import {Injectable, Inject} from '@angular/core';
import {AddLoaderItem, RemoveLoaderItem} from 'sofisa-loader';
import {NotificationService} from 'sofisa-notification';

@Injectable({
	providedIn: 'root'
})

export class AuthService {
	// TODO: Mover para arquivos de configurações
	apiUri: String = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';

	constructor(private oauthService: OAuthService, public loaderService: LoaderService, @Inject(Store) public store, public notificationService: NotificationService) {}
	
	init() {
		this.oauthService.configure({
			tokenEndpoint: `${this.apiUri}api/oauth-api/oauth/token`,
			redirectUri: window.location.origin,
			scope: '',
			showDebugInformation: true,
			oidc: true,
			requireHttps: false,
			clientId: 'trusted-app'
		});
	}
	
	isTokenValid() {
		return this.oauthService.hasValidAccessToken();
	}
	
	getAccessToken() {
		return this.oauthService.getAccessToken();
	}
	
	doLogin(user: { username: string, password; string }) {
		this.store.dispatch(new AddLoaderItem({
			page: 'login',
			name: 'login'
		}));
		
		this.oauthService.fetchTokenUsingPasswordFlow(user.username, user.password)
		.then((response: any) => {
			window.location.href = this.oauthService.redirectUri;
		})
		.catch((err) => {			
			this.notificationService.addMessage({
				type: 'error',
				message: 'Login ou senha incorreto! Tente novamente'
			});
		})
		.finally(() => {
			this.store.dispatch(new RemoveLoaderItem({
				page: 'login',
				name: 'login'
			}));
		});
	}
	
	doLogout() {
		this.oauthService.logOut(true);
	}
}
