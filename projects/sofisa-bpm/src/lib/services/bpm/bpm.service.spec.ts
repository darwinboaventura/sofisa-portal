import { TestBed } from '@angular/core/testing';

import { BpmService } from './bpm.service';

describe('BpmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BpmService = TestBed.get(BpmService);
    expect(service).toBeTruthy();
  });
});
