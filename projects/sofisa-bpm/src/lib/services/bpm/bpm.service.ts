import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})

export class BpmService {
	// TODO: Mover para arquivos de configurações
	apiUri: String = '//sofisa-lab-gateway.eastus2.cloudapp.azure.com';

	constructor(public http: HttpClient) {}
	
	claimTaskById(id: any, userId: any) {
		return this.http.post(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/claim`, {
			userId: userId
		});
	}
	
	completeTaskById(id: any, values: any) {
		return this.http.post(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/complete`, values);
	}
	
	getTaskById(id: any) {
		return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}`);
	}
	
	getTaskByIdVariables(id: any) {
		return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/task/${id}/variables`);
	}
	
	getTasks() {
		return this.http.get('${this.apiUri}/api/bpm-api/bpm-pj-digital/task');
	}
	
	getTaskDiagram(id: any) {
		return this.http.get(`${this.apiUri}/api/bpm-api/bpm-pj-digital/engine/default/process-definition/${id}/xml`);
	}
	
	startABpm(value: any) {
		return this.http.post('${this.apiUri}/api/bpm-api/bpm-pj-digital/process-definition/key/' +
			'Workflow_PJ_Digital/start', value);
	}
}
